
root := `git rev-parse --show-toplevel`
intera_shell_active := `env | grep -iq 'intera - h' && echo true || true`

launch: 
  test {{intera_shell_active}}
  roslaunch scan_trajectories background_nodes.launch simulated:=true
