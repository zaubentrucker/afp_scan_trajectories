import cv2 as cv
import numpy as np

with open("image.txt") as f:
  content = f.read()

# remove header
content = content.split(":")[-1]
# remove tailing `---`
content = content.split("\n")[0]
content = eval(content)
content = np.array(content, dtype=np.uint8)
content = content.reshape((480, -1))
cv.imwrite("image.bmp", content)

