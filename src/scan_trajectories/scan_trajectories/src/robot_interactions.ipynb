{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interacting with the Robot\n",
    "\n",
    "In order to execute the python code in this notebook, you need to start the jupyter-kernel\n",
    "from another shell that also ran `./intera.sh` (or `./intera.sh sim` for the simulation).\n",
    "So when using `vscode`, you could do:\n",
    "\n",
    "```bash\n",
    "cd /home/ros1/sawyer_ros_ws\n",
    "./intera.sh sim && sleep 1\n",
    "code .\n",
    "```\n",
    "\n",
    "Otherwise, the datastructures of the ros topics are not defined, making it impossible to read from them.\n",
    "\n",
    "## Starting the necessary nodes\n",
    "Using MoveIt for ROS, we can plan and execute movements of the robot arm.\n",
    "Before we can use it though, we need to set up a lot of ROS nodes that do the heavy lifting in the background.\n",
    "\n",
    "We created some .launch files for this. Launch files are the usual way to start a ROS application. You can find the files in the /launch-folder of this rospackage. For convenience, we also created a justfile at the caktin workspace root folder that can launch the simulation and required nodes.\n",
    "Just execute the following commands in a new shell:\n",
    "```bash\n",
    "cd /home/ros1/sawyer_ros_ws\n",
    "./intera.sh sim\n",
    "just launch\n",
    "```\n",
    "or call `roslaunch` directly:\n",
    "```bash\n",
    "cd /home/ros1/sawyer_ros_ws\n",
    "./intera.sh sim\n",
    "roslaunch scan_trajectories background_nodes.launch simulated:=$SIMULATED\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check whether the environment allows us to interact with the robot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import subprocess\n",
    "\n",
    "os.chdir(\"/home/ros1/sawyer_ros_ws/\")\n",
    "\n",
    "packages = subprocess.run([\"rospack\", \"list\"], capture_output=True)\n",
    "\n",
    "topics = subprocess.run([\"rostopic\", \"list\"], capture_output=True)\n",
    "assert topics.returncode == 0, \"We should be able to get the topics from the robot\"\n",
    "topics = topics.stdout.decode(\"UTF8\").splitlines()\n",
    "camera_config = list(\n",
    "    filter(lambda x: \"camera\" in x and \"config\" in x and \"head\" in x, topics)\n",
    ")\n",
    "assert len(camera_config) > 0, \"The robot should have a camera config topic\"\n",
    "print(f\"Found camera config topic at: {camera_config}\")\n",
    "print(\"All clear!\")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initializing the python interface\n",
    "To interface with the robot, we created a python package `scan_trajectories` that provide just the functionality that we need through a bunch of modules.\n",
    "The direct interactions with ROS are covered by\n",
    "- camera.py\n",
    "- collision.py\n",
    "- motion.py\n",
    "- visualization.py\n",
    "\n",
    "For 3D-Space operations we also provide\n",
    "- transformation.py\n",
    "\n",
    "All modules above are used to perform the scans in\n",
    "- scan_algorithm.py\n",
    "\n",
    "The `__init__.py` of `scan_trajectories` initializes ROS for us, so we can just import any module and use it without having to coordinate initialization ourselves."
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Moving the Robot\n",
    "With a `Motion` object we can read the current position of the arm and pose it:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from geometry_msgs.msg import Pose, Point\n",
    "from scan_trajectories.motion import Motion\n",
    "\n",
    "motion = Motion()\n",
    "original_pose = motion.current_pose\n",
    "\n",
    "target_pose = Pose(\n",
    "    position=Point(x=0.5, y=0, z=0.1),\n",
    "    orientation=original_pose.orientation\n",
    ")"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "assert motion.goto(target_pose)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The robot should now have moved to the specified pose.\n",
    "We can also use it to plan trajectories:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "trajectory = motion.plan(original_pose)\n",
    "print(f\"Trajectory would take {trajectory.joint_trajectory.points[-1].time_from_start.secs} seconds to complete\")\n",
    "start_time = time.time()\n",
    "motion.execute(trajectory)\n",
    "print(f\"It actually took {time.time() - start_time} seconds.\")"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "assert motion.goto(target_pose)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Visualizing with markers in Rviz\n",
    "Using a `Visualization` object, we can show arrows and coordinate systems in Rviz:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from scan_trajectories.visualization import Visualization, arrow_from_camera_pose\n",
    "\n",
    "visualization = Visualization()\n",
    "visualization.draw_coordinate_system(original_pose)\n",
    "\n",
    "# The camera points into z-direction, while the marker points into x-direction\n",
    "visualization.draw_marker(arrow_from_camera_pose(motion.current_pose))"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "And we can also delete the markers again:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "visualization.clear_all_markers()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Adding obstacles\n",
    "Using a `Collision` object, we can add geometry that the planner should consider when checking for collisions. For example, we can add a blocker for a table in front of the robot:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from scan_trajectories.collision import Collision\n",
    "\n",
    "collision = Collision()\n",
    "collision.add_table()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "We can also add the camera view cone to the collision such that the robot will avoid seeing itself:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "collision.add_camera_cone(0.3)"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "If you play around with the manual posing tool in Rviz, you'll see that it reports a collision if you try to pose the robot in a way that camera cone or table intersect with anything."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Getting Images\n",
    "As we have seen, we can subscribe to a specific rostopic to get the camera images as they are taken. Our `Camera` class registers a subscriber to the according topic and provides it to us:"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from scan_trajectories.camera import Camera\n",
    "\n",
    "camera = Camera()"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "image = camera.get_next_image()\n",
    "Camera.save_image(image, \"image.jpg\")"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "767d51c1340bd893661ea55ea3124f6de3c7a262a8b4abca0554b478b1e2ff90"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
