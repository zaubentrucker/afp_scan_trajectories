from typing import Optional, Any

import rospy
import moveit_msgs.msg
from geometry_msgs.msg import Pose, Transform
from moveit_commander import MoveGroupCommander
from moveit_msgs.msg import RobotState, RobotTrajectory
from sensor_msgs.msg import JointState


class Motion:
    """
    Provide a planning and movement interface
    """

    def __init__(self):
        print("initializing motion", flush=True)
        self._move_group = MoveGroupCommander("right_arm")
        self._display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        # the sawyer seems to be less accurate than the standard setting
        self._move_group.set_goal_tolerance(0.001)

    def goto(self, pose: Pose) -> bool:
        """
        Plan a trajectory to a pose and execute that plan.

        The Method blocks until either the pose is reached, or an error occurs

        :param pose: The goal pose to plan towards
        :return: `True` if the pose was reached, `False` otherwise
        """
        self._move_group.clear_pose_targets()
        trajectory = self.plan(pose)
        if trajectory is None:
            return False

        return self.execute(trajectory)

    def execute(self, trajectory: RobotTrajectory) -> bool:
        """
        Move the robot from the current position along the trajectory
        :param trajectory: The trajectory to follow
        :return: `True` if the motion succeeded, `False` otherwise
        """
        success = self._move_group.execute(trajectory)
        self._move_group.stop()
        return success

    def plan(
        self,
        goal_pose: Pose,
        start_state: Optional[JointState] = None,
        planning_time: float = 0.1,
    ) -> Optional[RobotTrajectory]:
        """
        Try to plan a trajectory to the given pose goal
        :param goal_pose: The pose that should be reached after moving through the trajectory
        :param start_state: If given, the trajectory will be planned from here
        :param planning_time: The duration in seconds that the planner may take for planning
        :return:
        """
        if start_state is None:
            start_state = self._move_group.get_current_state()

        self._move_group.set_start_state(start_state)
        self._move_group.set_pose_target(goal_pose)
        self._move_group.set_planning_time(planning_time)

        success, trajectory, planning_time, error_code = self._move_group.plan()

        return trajectory if success else None

    @property
    def current_pose(self) -> Pose:
        return self._move_group.get_current_pose().pose

    @property
    def current_state(self) -> RobotState:
        return self._move_group.get_current_state()
