import rospy
from sensor_msgs.msg import Image
import threading
import numpy as np
import cv2


class Camera:
    def __init__(self):
        """
        Registers a subscriber for the wrist camera image topic and thus allows
        for retrieving incoming images.
        """
        self._image_acquired_condition = threading.Condition()
        self._last_image = None

        def update_image(image: Image):
            with self._image_acquired_condition:
                self._last_image = image
                self._image_acquired_condition.notify_all()

        self._image_subscriber = rospy.topics.Subscriber(
            "/io/internal_camera/right_hand_camera/image_rect",
            Image,
            callback=update_image,
        )

    def get_next_image(self) -> Image:
        """
        Wait for the next image from the right hand camera and return it
        """
        with self._image_acquired_condition:
            self._image_acquired_condition.wait()
            image = self._last_image
        return image

    @classmethod
    def save_image(cls, image: Image, path: str):
        """
        Save the given `image` to disk
        """
        image = np.array(list(image.data), dtype=np.uint8)
        image = image.reshape((480, -1))
        cv2.imwrite(path, image)

    def __del__(self):
        self._image_subscriber.unregister()
