from copy import deepcopy
from typing import List

from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from rospy import Duration
from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import Marker
from .collision import Collision
from .motion import Motion
from .visualization import Visualization, arrow_to_camera_pose
from .camera import Camera
import abc
import math
import numpy as np
import logging
from tf import transformations
from .transformations import point_from_np, point_to_np, quat_from_np, quat_to_np


CAMERA_ROTATION_RESOLUTION = 8


class Scan(abc.ABC):
    def __init__(
        self,
        scan_poses: List[Pose],
        object_position: Point,
        visualization: Visualization,
        camera: Camera,
        motion: Motion,
        collision: Collision,
    ):
        """
        Common interface for creating, visualizing and performing scans

        :param scan_poses: A list of poses that the robot should scan from.
            The images will be taken into positive x-direction of the given pose.
            The algorithm may adjust the rotation about the x-axis and thus the
            image orientation of the camera image in order to optimize movement
            of the robot.
        """
        self.visualization = visualization
        self.camera = camera
        self.motion = motion
        self.scan_poses = scan_poses
        self.object_position = object_position
        self.collision = collision

    def visualize(self):
        """
        Draw visualization markers into the scene:
         - Arrows for the camera poses
         - A sphere for the target object position
        """
        for pose in self.scan_poses:
            self.visualization.draw_marker(pose, scale=Vector3(x=0.06, y=0.015, z=0.015))

        target_object_pose = Pose()
        target_object_pose.orientation.w = 1
        target_object_pose.position = self.object_position
        target_color = ColorRGBA(a=1.0, r=0.5, g=0.5, b=0.5)
        self.visualization.draw_marker(
            target_object_pose,
            Marker.SPHERE,
            Vector3(x=0.05, y=0.05, z=0.05),
            target_color,
        )

    def perform(self, image_folder):
        def duration_to_seconds(d: Duration) -> float:
            return d.secs + d.nsecs/10**9

        for index, pose in enumerate(self.scan_poses):
            robot_pose = arrow_to_camera_pose(pose)
            distance_to_object = np.linalg.norm(
                point_to_np(pose.position) - point_to_np(self.object_position)
            )
            self.collision.add_camera_cone(distance_to_object)

            fastest_trajectory = min(
                filter(
                    lambda x: x is not None,
                    map(
                        lambda x: self.motion.plan(x),
                        map(
                            arrow_to_camera_pose,
                            Scan._x_rotations(pose, CAMERA_ROTATION_RESOLUTION),
                        ),
                    ),
                ),
                key=lambda x: duration_to_seconds(x.joint_trajectory.points[-1].time_from_start),
                default=None,
            )
            if fastest_trajectory is None:
                print(f"Unable to plan to {robot_pose}")
                continue
            else:
                print(
                    f"Found fastest trajectory taking "
                    f"{duration_to_seconds(fastest_trajectory.joint_trajectory.points[-1].time_from_start)} "
                    f"seconds"
                )

            success = self.motion.execute(fastest_trajectory)

            if success:
                image = self.camera.get_next_image()
                self.camera.save_image(image, f"{image_folder}/{index}.jpg")
            else:
                print(f"Error during trajectory execution: {robot_pose}")
            self.collision.remove_camera_cone()

    @staticmethod
    def _x_rotations(pose: Pose, count: int) -> List[Pose]:
        """
        Generate poses that are equal but for rotation around their x-axis.

        :param pose: The original position and rotation
        :param count: The amount of poses to generate
        :return: A list of poses with the same position that are rotated around their x-axis
        """
        pose = deepcopy(pose)
        res = []
        for i in range(count):
            phi = i * math.tau / count
            rotation = transformations.quaternion_from_euler(phi, 0, 0)
            orientation = quat_to_np(pose.orientation)
            orientation = transformations.quaternion_multiply(orientation, rotation)
            orientation = quat_from_np(orientation)
            res.append(Pose(position=pose.position, orientation=orientation))

        return res


def scan_poses_ring(
    target_position: Point,
    z_offset: float,
    radius: float,
    position_count: int,
) -> List[Pose]:
    """
    Generate a list of poses that form a ring that is centered around the
    `target_position` and that all point with their x-direction towards
    `target_position`.

    :param target_position: Position around which the poses will be centered
    :param z_offset: A z offset that will be applied to the ring
    :param radius: Radius of the ring
    :param position_count: The length of the generated list of poses
    """
    assert position_count > 0

    def pos_array(p: Point) -> np.ndarray:
        return np.array([p.x, p.y, p.z])

    poses = []
    for i in range(position_count):
        phi = i * (math.tau / position_count)
        """rotation around z axis of the position"""

        position = Point(
            x=target_position.x + math.sin(phi) * radius,
            y=target_position.y + math.cos(phi) * radius,
            z=target_position.z + z_offset,
        )

        yaw = transformations.quaternion_from_euler(0, 0, -phi - math.tau / 4)

        hypotenuse = np.linalg.norm(
            np.subtract(pos_array(target_position), pos_array(position))
        )
        adjacent = radius
        pitch_angle = math.acos(adjacent / hypotenuse)
        pitch = transformations.quaternion_from_euler(0, pitch_angle, 0)

        orientation = transformations.quaternion_from_euler(0, 0, 0)
        orientation = transformations.quaternion_multiply(pitch, orientation)
        orientation = transformations.quaternion_multiply(yaw, orientation)
        orientation = Quaternion(
            x=orientation[0], y=orientation[1], z=orientation[2], w=orientation[3]
        )
        poses.append(Pose(position=position, orientation=orientation))

    return poses
