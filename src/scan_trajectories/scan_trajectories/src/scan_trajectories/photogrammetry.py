#Nach https://github.com/agisoft-llc/metashape-scripts/blob/master/src/samples/general_workflow.py

import Metashape
import os, time


def start_photogrammetry(image_folder, output_folder):
   
   
    # Checking compatibility
    compatible_major_version = "2.0"
    found_major_version = ".".join(Metashape.app.version.split('.')[:2])
    if found_major_version != compatible_major_version:
        raise Exception("Incompatible Metashape version: {} != {}".format(found_major_version, compatible_major_version))

    def find_files(folder, types):
        return [entry.path for entry in os.scandir(folder) if (entry.is_file() and os.path.splitext(entry.name)[1].lower() in types)]

    #Fotos aus dem Input-Ordner 
    photos = find_files(image_folder, [".jpg", ".jpeg", ".tif", ".tiff"])

    #Photogrammtery-Projekt erstellen
    doc = Metashape.Document()
    doc.save(output_folder + '/project.psx')

    #Ein Chunk in dem Projekt hinzufügen
    #Chunk = Gruppierung von Fotos die für die Photogrammetry benutzt werden.
    chunk = doc.addChunk()          

    #Fotos aus dem Input-Ordner in den Chunk laden
    chunk.addPhotos(photos)
    doc.save()

    #Konsolenausgabe: Anzahl der Fotos/Cameras + " images loaded"
    print(str(len(chunk.cameras)) + " images loaded")

    


    #Benutzereingabe um die gewünschte Qualität der Photogrammetrie zu erfassen
    qualitys = ["High", "Medium", "Low"]

    # # Ausgabe der Optionen
    # print("Wählen Sie eine Option aus:")
    # for index, quality in enumerate(qualitys):
    #     print(f"{index + 1}. {quality}")
    #
    # # Lesen der Benutzereingabe
    # while True:
    #     try:
    #         choice = int(input("Ihre Auswahl: "))
    #         if choice < 1 or choice > len(qualitys):
    #             raise ValueError()
    #         break
    #     except ValueError:
    #         print("Ungültige Eingabe. Bitte geben Sie eine Zahl zwischen 1 und", len(qualitys), "ein.")
    #
    # # Verarbeiten der Auswahl
    # selected_quality = qualitys[choice - 1]
    # print("Sie haben", selected_quality, "gewählt.")


    selected_quality = qualitys[0]
    #Zeitmessung-Timer startet
    start_time = time.time()




    #keypoint_limit = Anzahl der erkannten Merkmale pro Bild. Je höher desto genauer
    #downscale = Faktor um den die Auflösung verringert wird. 2 -> neue Auflösung entspricht 50% der originalen Auflösung
    #tiepoint_limit = Punkte im 3D-Model die den Merkmalen auf den Fotos entsprechen. Je höher desto genauer
    if selected_quality == "High":
        chunk.matchPhotos(keypoint_limit = 40000, tiepoint_limit = 10000, generic_preselection = True, reference_preselection = True)
    elif selected_quality == "Medium":
        chunk.matchPhotos(downscale= 2,keypoint_limit = 20000, tiepoint_limit = 5000, generic_preselection = True, reference_preselection = True)
    elif selected_quality == "Low":
        chunk.matchPhotos(downscale= 3, keypoint_limit = 1000, tiepoint_limit = 1000, generic_preselection = True, reference_preselection = True)

    doc.save()

    chunk.alignCameras()
    doc.save()

    chunk.buildDepthMaps(downscale = 2, filter_mode = Metashape.MildFiltering)
    doc.save()

    chunk.buildModel(source_data = Metashape.DepthMapsData)
    doc.save()

    # chunk.buildUV(page_count = 2, texture_size = 4096)
    # doc.save()

    # chunk.buildTexture(texture_size = 4096, ghosting_filter = True)
    # doc.save()

    # has_transform = chunk.transform.scale and chunk.transform.rotation and chunk.transform.translation

    # if has_transform:
    #     chunk.buildPointCloud()
    #     doc.save()

    #     chunk.buildDem(source_data=Metashape.PointCloudData)
    #     doc.save()

    #     chunk.buildOrthomosaic(surface_data=Metashape.ElevationData)
    #     doc.save()

    # export results
    chunk.exportReport(output_folder + '/report.pdf')

    if chunk.model:
        chunk.exportModel(output_folder + '/model.obj')

    # if chunk.point_cloud:
    #     chunk.exportPointCloud(output_folder + '/point_cloud.las', source_data = Metashape.PointCloudData)

    # if chunk.elevation:
    #     chunk.exportRaster(output_folder + '/dem.tif', source_data = Metashape.ElevationData)
# 
    # if chunk.orthomosaic:
    #     chunk.exportRaster(output_folder + '/orthomosaic.tif', source_data = Metashape.OrthomosaicData)


    #Zeitmessung-Timer beenden
    end_time = time.time()
    execution_time = (end_time - start_time) / 60

    print('Processing finished in {:.2f} minutes, results saved to '.format(execution_time) + output_folder + '.')
    
