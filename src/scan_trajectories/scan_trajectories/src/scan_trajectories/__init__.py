import moveit_commander
import sys
import rospy

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("scan_trajectories", anonymous=True)