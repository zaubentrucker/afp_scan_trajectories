import math
from copy import deepcopy
from typing import Optional

from geometry_msgs.msg import Pose, Vector3, Quaternion, Point
from rospy import Publisher, Time
from visualization_msgs.msg import Marker
from std_msgs.msg import ColorRGBA
import numpy as np
from tf.transformations import quaternion_from_euler, quaternion_multiply


def euler_rotated(q: Quaternion, ai: float, aj: float, ak: float) -> Quaternion:
    arr = quat_to_np(q)
    rot = quaternion_from_euler(ai, aj, ak)
    res = quaternion_multiply(arr, rot)
    return quat_from_np(res)


def quat_to_np(q: Quaternion) -> np.ndarray:
    return np.array([
        q.x,
        q.y,
        q.z,
        q.w
    ])


def quat_from_np(a: np.ndarray) -> Quaternion:
    return Quaternion(
        x=a[0],
        y=a[1],
        z=a[2],
        w=a[3],
    )


def point_to_np(p: Point) -> np.ndarray:
    return np.array([
        p.x,
        p.y,
        p.z,
    ])


def point_from_np(a: np.ndarray) -> Point:
    return Point(
        x=a[0],
        y=a[1],
        z=a[2],
    )


