import math
import time
from copy import deepcopy
from typing import Optional

from geometry_msgs.msg import Pose, Vector3, Quaternion
from rospy import Publisher, Time
from visualization_msgs.msg import Marker
from std_msgs.msg import ColorRGBA
import numpy as np
from tf.transformations import quaternion_from_euler, quaternion_multiply
from .transformations import quat_from_np, quat_to_np

_markers_drawn = 0
"""
The amount of markers that have already been drawn. Used for indexing the markers
"""


class Visualization:
    def __init__(self):
        self.publisher = Publisher("visualization_marker", Marker, queue_size=0)
        # Wait a bit for the publisher to work?
        # TODO find a better solution for this
        time.sleep(0.5)

    def draw_coordinate_system(self, pose: Pose):
        def quaternion_from_array(q: np.ndarray):
            return Quaternion(
                x=q[0],
                y=q[1],
                z=q[2],
                w=q[3],
            )

        pose = deepcopy(pose)

        orig_orienation = np.array([
            pose.orientation.x,
            pose.orientation.y,
            pose.orientation.z,
            pose.orientation.w,
        ])

        self.draw_marker(pose, color=ColorRGBA(a=1, r=1))
        y_rotation = quaternion_from_euler(0, 0, math.tau / 4)
        y_orientation = quaternion_multiply(orig_orienation, y_rotation)
        z_rotation = quaternion_from_euler(0, -math.tau / 4, 0)
        z_orientation = quaternion_multiply(orig_orienation, z_rotation)

        pose.orientation = quaternion_from_array(y_orientation)
        self.draw_marker(pose, color=ColorRGBA(a=1, g=1))
        pose.orientation = quaternion_from_array(z_orientation)
        self.draw_marker(pose, color=ColorRGBA(a=1, b=1))

    def draw_marker(
            self,
            pose: Pose,
            marker_type: Optional[int] = Marker.ARROW,
            scale: Optional[Vector3] = None,
            color: Optional[ColorRGBA] = None,
    ) -> int:
        """
        Draw a marker in Rviz. In order to see the marker, there must be a
        "Marker"-Display in the Rviz scene

        :param pose: The position and orientation of the marker
        :param marker_type: The shape of the marker. Must be in:
            [Marker.ARROW, Marker.CUBE, Marker.CYLINDER, Marker.SPHERE]
        :param scale: Scale of the marker in x, y and z
        :param color: Color of the marker

        :returns: The id of the marker that was drawn. Can be used to delete a
            specific Marker via `delete_marker()`
        """
        global _markers_drawn

        marker = Visualization._default_marker()
        marker.action = Marker.ADD
        marker.id = _markers_drawn
        marker.pose = pose

        if scale is not None:
            marker.scale = scale

        if color is not None:
            marker.color = color

        if marker_type is not None:
            marker.type = marker_type

        _markers_drawn += 1
        self.publisher.publish(marker)
        return marker.id

    def delete_marker(self, marker_id: int):
        """
        Delete a specific marker from the scene

        :param marker_id: The id of the marker
        """
        marker = Visualization._default_marker()
        marker.action = Marker.DELETE
        marker.id = marker_id
        self.publisher.publish(marker)

    def clear_all_markers(self):
        """
        Delete all markers from the scene
        """
        marker = Visualization._default_marker()
        marker.action = Marker.DELETEALL
        self.publisher.publish(marker)

    @staticmethod
    def _default_marker() -> Marker:

        marker = Marker(
            type=Marker.ARROW,
            scale=Vector3(x=0.1, y=0.03, z=0.03),
            color=ColorRGBA(a=1.0, r=0.0, g=1.0, b=0.0),
        )
        marker.header.frame_id = "base"
        marker.header.stamp = Time.now()
        marker.ns = ""
        return marker


_ARROW_TO_CAMERA_QUAT = quaternion_from_euler(0, math.tau / 4, 0)


def arrow_from_camera_pose(camera_pose: Pose) -> Pose:
    """
    Convert the camera direction to a pose of an arrow marker

    The camera is pointing into the z-direction of the last joint, while the markers
    are pointing into their x-direction.
    """
    camera_pose = deepcopy(camera_pose)

    orientation = quat_to_np(camera_pose.orientation)
    rotation = _ARROW_TO_CAMERA_QUAT.copy()
    rotation[3] *= -1
    orientation = quaternion_multiply(orientation, rotation)
    camera_pose.orientation = quat_from_np(orientation)
    return camera_pose


def arrow_to_camera_pose(arrow_pose: Pose) -> Pose:
    """
    Convert a pose of an arrow marker to the camera direction

    The camera is pointing into the z-direction of the last joint, while the markers
    are pointing into their x-direction.
    """
    arrow_pose = deepcopy(arrow_pose)

    orientation = quat_to_np(arrow_pose.orientation)
    orientation = quaternion_multiply(orientation, _ARROW_TO_CAMERA_QUAT)
    arrow_pose.orientation = quat_from_np(orientation)
    return arrow_pose
