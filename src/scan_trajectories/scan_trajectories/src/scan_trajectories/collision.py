import math
from copy import deepcopy

import geometry_msgs
from geometry_msgs.msg import Pose, PoseStamped, Point
from moveit_commander import PlanningSceneInterface
from rospy import Time
from moveit_msgs.msg import AttachedCollisionObject, CollisionObject
from shape_msgs.msg import SolidPrimitive
from std_msgs.msg import Header
from .transformations import quat_from_np, quat_to_np, point_from_np, point_to_np
from tf.transformations import (
    euler_from_quaternion,
    rotation_matrix,
    quaternion_matrix,
    quaternion_multiply,
    quaternion_from_euler,
)
import numpy as np


class Collision:
    def __init__(self):
        self._planning_scene = PlanningSceneInterface()

    def add_table(self):

        pose = Pose()
        pose.orientation.w = 1.0
        # TODO: use tf2 and calculate this from world transform and table geom.
        TABLE_HEIGHT = 0.755 + 0.04 / 2
        pose.position.z = -1 - 0.93 + TABLE_HEIGHT

        co = CollisionObject(
            operation=CollisionObject.ADD,
            id="table",
            header=Header(
                frame_id="base",
            ),
            pose=pose,
            primitives=[
                SolidPrimitive(
                    type=SolidPrimitive.BOX, dimensions=[2, 2, 2]
                )
            ],
        )

        co.operation = co.ADD

        self._planning_scene.add_object(co)

    def remove_table(self):
        self._planning_scene.remove_attached_object("base", "table")
        self._planning_scene.remove_world_object("table")

    def add_camera_cone(
        self,
        cone_length: float,
        cone_angle: float = math.tau / 16,
    ):
        """
        Add a cone that represents the view from the camera until it hits the object
        :param camera_pose: Pose of the camera looking into the x-direction
        :param cone_length: Position of the target
        :param cone_angle: Angle of the view cone in radians
        """

        cone_radius = cone_length * math.tan(cone_angle)

        pose = Pose()
        pose.orientation = quat_from_np(quaternion_from_euler(math.pi, 0, 0))
        pose.position.z += cone_length / 2

        co = CollisionObject(
            operation=CollisionObject.ADD,
            id="camera_cone",
            header=Header(
                frame_id="right_hand_camera",
            ),
            pose=pose,
            primitives=[
                SolidPrimitive(
                    type=SolidPrimitive.CONE, dimensions=[cone_length, cone_radius]
                )
            ],
        )

        aco = AttachedCollisionObject()
        aco.link_name = "right_hand_camera"
        aco.touch_links = [
            "right_hand_camera",
            "right_l6",
            "right_j6",
            "right_l5",
            "right_j5",
        ]

        aco.object = co

        self._planning_scene.attach_object(aco)

    def remove_camera_cone(self):
        self._planning_scene.remove_attached_object("right_hand_camera", "camera_cone")
        self._planning_scene.remove_world_object("camera_cone")
