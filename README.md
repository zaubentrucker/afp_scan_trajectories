## Dependencies
The following dependencies must be available on your machine
- python
  - [agisoft python module](https://www.agisoft.com/downloads/installer/)
- ROS noetic
  - You can follow along with the [Workstation Setup](https://sdk.rethinkrobotics.com/intera/Workstation_Setup) 
    tutorial
- The [just](https://github.com/casey/just) command runner
## Installation
1. Clone the project:
    ```bash
    git clone --recursive https://git.rwth-aachen.de/zaubentrucker/afp_scan_trajectories.git
    ```
2. Build the workspace
    ```bash
    cd afp_scan_trajectories
    catkin_make
    ```
   
## Initializing the ROS nodes
There is a `justfile` that provides a `launch` command:
```bash
./intera.sh sim
just launch
```

## Running the Jupyter Notebooks
There are a few Jupyter notebooks that demonstrate the capabilities of our python modules.
To run them, enter the intera environment, switch to their directory and start your 
jupyter kernel from there.

Here, I just start vscode:
```bash
./intera.sh sim
cd src/scan_trajectories/scan_trajectories/src
code .
```
With the jupyter extensions for vscode installed, and the ROS-nodes initialized, you should
be able to run the cells.